const urlAtd = 'https://gitlab.com/atd-sdop/awesome-atd/-/blob/master/';
const urlRaw = 'https://gitlab.com/atd-sdop/awesome-atd/-/raw/master/README.md';

function getRaw(url){

fetch(url)
    .then(res => res.text())
    .then(data => { 
        let linhas = data.split(/\r?\n/);
        let link = '<ul>';
        linhas.forEach(function(linha){
            if (/\[(.+)\]\((.+)\)/.test(linha))
                    link += linha.replace(/.+\[(.+)\]\((.+)\)/, "<li><a target='_blank' href='"+urlAtd+"$2'> $1</a></li>"); 
        })
        return link + '</ul>';
    })
    .then(links => { 
        if (links == '<ul></ul>') throw new UserException("não encontrou a pagina ou não logado ou não tem permissão");
        document.getElementById('rank').innerHTML = links
    })
    .catch(function(err) {
        console.info(err);
        document.getElementById('rank').innerHTML = '<br><span class="red">NÃO LOGADO!</span>';

    });
}

getRaw(urlRaw);


// Contribuições Git Lab
const urlGitlab = 'https://gitlab.com/'; //${user}
// title = document.querySelector('[direction="ltr"]').previousSibling.children[0].getAttribute("title")
// user = document.querySelector('[data-user]').getAttribute('data-user')

function getUser(url) { 
    fetch(url)
        .then(res => res.text())
        .then(text => {
            let fragment = document.createElement('div');
            fragment.innerHTML = text;
            return fragment;
        })
        .then(fragment => {
            let user = fragment
                .querySelector('[data-user]').getAttribute('data-user');
            let avatar = fragment
                .querySelector('.header-user-avatar').getAttribute('data-src');
            let name = fragment
                .querySelector('.header-user-avatar').getAttribute('alt');
            getJson(`${url}users/${user}/calendar.json`)
            document.getElementById('user').innerHTML = `<p><a target="_blank" href="${url+user}"><img src="${url+avatar}" class="avatar"/> ${name}</a></p>`;
        })
        .catch(function(err) {
            console.info(err);            
        });
}

getUser(urlGitlab);


const agora = new Date()
const hoje = agora.toISOString().split('T')[0]

function getJson(url) {
    console.log('dados', url)
    fetch(url)
        .then(res => res.json())
        .then(json => {
            let total = ( json[hoje] )? json[hoje] : 0 ;
            document.getElementById('numero').innerHTML = `<span class="numero" title="contribuições hoje"> ${total}</span>`;
        })
        .catch( err => console.info(err));
}
